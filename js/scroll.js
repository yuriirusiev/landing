$('a[href^="#"]').on('click',function (e) {
    e.preventDefault();
    var id = $(this).attr('href'),
        top = $(id).offset().top-$('.header__inner').height();;


    $('body,html').animate({
        scrollTop: top
    }, 600);
});